'use strict';

var assemble = require( 'assemble' );
var helpers = require( 'handlebars-helpers' );

var extname = require( 'gulp-extname' );
var watch = require( 'base-watch' );
var merge = require('merge');
var fs = require('fs');

var files = {
  layout: 'src/layout/*.hbs',
  pages: 'src/pages/*.hbs',
  posts : 'src/posts/*.hbs',
  team_members : 'src/team/*.hbs',
  partials: 'src/partials/**/*.hbs',
  data: [ 'src/data/*.{json,yml}', 'src/assets/**/*.{json,yml}' ],
};
var options = {
      collections: [
        {
          name: 'posts',
          inflection: 'post',
          sortorder: 'desc',
          sortby: 'date'
        },
        {
          name: 'team_members',
          inflection: 'member',
          sortorder: 'desc',
          sortby: 'order'
        },
        {
          name: 'tags',
          inflection: 'tag',
        }
      ]
    };

var app = assemble(options);
app.use( watch() );
app.helpers( helpers() );

app.clear();

app.partials( files.partials );
app.layouts( files.layout );
app.data( files.data );
app.pages( files.pages );

app.create('posts');
app.create('team_members');

app.posts( files.posts );
app.team_members( files.team_members );
var tags = app.team_members.groupBy('data.tags');

app.task('build-pages', () => {
  var post_pages = app.list(app.posts).paginate({limit: 10});

  return app.toStream('pages')
    .pipe( app.renderFile({post_pages : post_pages, tags: tags}) )
    .pipe( extname('.html') )
    .pipe( app.dest( 'dist/' ) );
});
app.task('build-posts', () => {
  return app.posts.toStream()
    .pipe( app.renderFile())
    .pipe( extname('.html') )
    .pipe( app.dest( 'dist/posts/') );
});

app.task('assets', function() {
  // return, to let assemble know when the task has completed
  return app.copy('src/assets/**', process.env.npm_package_config_assetpath);
} );
app.task('build', ['build-pages', 'build-posts']);

app.task( 'watch', function() {
  app.data([ 'src/data/**/**.{json,yml}', 'src/assets/**/*.{json,yml}' ]);
  // app.watch( ['src/**/**.hbs'], [ 'build','assets'] );
  // app.watch( ['src/**/**.hbs','src/**/**.scss'], [ 'build','assets'] );
  app.watch( ['src/**/**.{hbs, scss}'], [ 'build','assets'] );
} );

app.task( 'default', [ 'build', 'assets' ] );
module.exports = app;

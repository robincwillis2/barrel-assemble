# Assemble Static Site Generator

## Table of Contents
1. [Getting Started](#getting-started)
2. [Developing](#developing)
2. [Build Scripts](#using-build-scripts)

## Getting Started

#### Intial Setup
Run this command from your project root directory to download the project's dependencies:
```
npm i
```

#### Install Lando
https://github.com/lando/lando/releases/tag/v3.0.0-beta.47

#### Start Lando Server

```
lando start
```

#### Copy Files
IF this is your first time starting up the theme, you'll need to run the following to copy all existing files from `src` to `dist`:
```bash
npm run build


## Developing
After the steps above, you are ready to go. The main commands needed to generate the `dist/` are `npm run build` and `npm run assemble:build` (in two separate terminal tabs). For more specific tasks see [Build Scripts](#using-build-scripts) for more information.
```

## Lando
Run `lando start` in the root once it is setup.

## Using Build Scripts
There are a variety of build scripts provided in the `package.json` that can be run directly in your terminal window. All scripts can be run via `npm run <scriptName>`, with the exception of `npm start`.

#### `npm start`
Runs a watch script on you CSS and JS paths, as well as all files. Does not copy any files, but does build CSS and JS once before starting the watch script.

### `npm run watch`
Watches for CSS,JS and Assemble template changes.

### `npm run webpack:watch`
Watches for CSS,JS changes.


#### Update Environment
Make sure your node environment is updated. You're using Homebrew for this, right?

```bash
# check kegs and brew version
brew update

# update node
brew upgrade node
```
var $ = require('jquery');

function Header(el) {
  var t = this;
	window.addEventListener('scroll',  function() {
		if ($(document).scrollTop() < 65){
			el.classList.remove('scrolled');
		}else{
			el.classList.add('scrolled');
		}
	});
	$(el).find('.menu-icon').click(function(){
		$('body,html').toggleClass('nav-open');
	});
}

module.exports = Header

var $ = require('jquery');
var YoutubePlayer = require('youtube-player');

function youtube(el) {
  var t = this;
  var id = $(el).find('.video').attr('data-video-id');
  var videoAreaId = $(el).find('.video').attr('id');
  var player =  YoutubePlayer(videoAreaId, 
  {
    playerVars: {
      autoplay: 0,           // Auto-play the video on load
      controls: 1,            // Show pause/play buttons in player
      showinfo: 0,            // Hide the video title
      modestbranding: 1,      // Hide the Youtube Logo
      loop: 0,                // Run the video in a loop
      fs: 1,                  // Hide the full screen button
      cc_load_policy: 0,      // Hide closed captions
      iv_load_policy: 3,      // Hide the Video Annotations
      autohide: 1,            // Hide video controls when playing
      rel: 0,
      color: 'white'
    }
  });

  player.loadVideoById(id);
  player.stopVideo();

  $(el).find('.play-button').click(function () {
		player.playVideo();
		$(el).addClass('video-playing');
  });

  $(el).find('.play-button').click(function () {
		player.playVideo();
		$(el).addClass('video-playing');
  });

  player.on('stateChange', function (event) {
    if (event.data == 0) {
    	player.stopVideo();
    	$(el).removeClass('video-playing');
    }
  });
}

module.exports = youtube
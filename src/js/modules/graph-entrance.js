var $ = require('jquery');
var scrollMonitor = require("scrollmonitor");

function graphEntrance(el) {

	var elementWatcher = scrollMonitor.create( el, -300 );

	elementWatcher.enterViewport(function() {
	  $(el).addClass('in-view');
	});
}

module.exports = graphEntrance
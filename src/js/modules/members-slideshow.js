var $ = require('jquery')
var slick = require('slick-carousel')

function membersSlideshow(el) {
  var t = this;
  t.$el = $(el);
  options = {
		prevArrow: ".arrow-prev",
    nextArrow: ".arrow-next",
	  fade:true
	};
    // t.$el.slick(options);
	$('body').append('<div class="member-slideshow"><div class="member-slides"></div></div>');
  $('.member').clone().appendTo($(".member-slideshow .member-slides"));
  $(".members .member-snippet").each(function(i,v){
  	$(this).attr('data-index', i);		
  });
  $(".member-slideshow .member").append('<div class="arrow-prev"><i class="material-icons">arrow_back</i></div><div class="arrow-next"><i class="material-icons">arrow_forward</i></div><div class="member-close"><i class="material-icons">close</i></div>');
  $(".member-slideshow .member-slides").slick(options);
  $('.member-slideshow .member').click(function(e){
  	e.preventDefault();
  	e.stopPropagation();
  })
   $('.member-slideshow .member .arrow-prev').click(function(e){
  	e.preventDefault();
  	e.stopPropagation();
  	$(".member-slideshow .member-slides").slick('slickPrev');
  })
   $('.member-slideshow .member .arrow-next').click(function(e){
  	e.preventDefault();
  	e.stopPropagation();
  	$(".member-slideshow .member-slides").slick('slickNext');
  })
  $('.member-slideshow,.member-close').click(function(){
  	$('.member-slideshow').removeClass('open');
  });
  $('.members .member-snippet').click(function(){
			$(".member-slideshow .member-slides").slick('slickGoTo', $(this).attr('data-index'));
			$(".member-slideshow").addClass('open')
  });
  


}

module.exports = membersSlideshow

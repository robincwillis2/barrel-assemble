var $ = require('jquery')
var slick = require('slick-carousel')

function Carousel(el) {
  var t = this;
  t.$el = $(el);
  var options = {};
  if (t.$el.data('featured')){
	  options = {
			prevArrow: ".featured-prev,.arrow-prev",
	    nextArrow: ".featured-next,.arrow-next",
		  slidesToShow: 3,
  		centerMode: false,
  		infinite: true,
		  padding: '0',
		  centerPadding: '0px',
		  slidesToScroll: 1,
		  rows:0,
		  responsive: 
	    [
	      {
	        breakpoint: 860,
	        settings: {
	        	centerMode: true,
	          slidesToShow: 1,
	          slidesToScroll: 1,
	          centerPadding: '25%',
	        }
	      },
	      {
	        breakpoint: 480,
	        settings: {
	        	centerMode: true,
	          slidesToShow: 1,
	          slidesToScroll: 1,
	          centerPadding: '25%',
	        }
	      }
	    ]
		};
	};

  if( t.$el.children.length > 1 ) {
    t.$el.slick(options);
  }

}

module.exports = Carousel
